package pl.silesiakursy.grade;

import java.util.Scanner;

public class Grade {
    public static void main(String[] args) {
        firstSolution();
        secondSolution();
    }

    public static void firstSolution() {
        System.out.println("--- Wersja 1 ---");
        int points = getTestResult();

        if (points < 0) {
            System.out.println("Ilość punktów mniejsza niż 0");
        } else if (points > 100) {
            System.out.println("Ilość punktów większa niż 100");
        } else if (points <= 39) {
            System.out.println("Wynik: ocena niedostateczna");
        } else if (points >= 40 && points <= 54) {
            System.out.println("Wynik: ocena dopuszczająca");
        } else if (points >= 55 && points <= 69) {
            System.out.println("Wynik: ocena dostateczna");
        } else if (points >= 70 && points <= 84) {
            System.out.println("Wynik: ocena dobra");
        } else if (points >= 85 && points <= 98) {
            System.out.println("Wynik: ocena bardzo dobra");
        } else {
            System.out.println("Wynik: ocena celująca");
        }
    }

    public static void secondSolution() {
        System.out.println("--- Wersja 2 ---");
        int points = getTestResult();

        if (points < 0) {
            System.out.println("Ilość punktów mniejsza niż 0");
        } else if (points > 100) {
            System.out.println("Ilość punktów większa niż 100");
        } else {
            System.out.println("Wynik: " + getGrade(points));
        }
    }

    public static int getTestResult() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podal ilość punktów uzyskanych na teście:");
        int testResult = scanner.nextInt();
        return testResult;
    }

    public static String getGrade(int points) {
        String grade = "ocena niedostateczna";
        if (points >= 40 && points <= 54) {
            grade = "ocena dopuszczająca";
        } else if (points >= 55 && points <= 69) {
            grade = "ocena dostateczna";
        } else if (points >= 70 && points <= 84) {
            grade = "ocena dobra";
        } else if (points >= 85 && points <= 98) {
            grade = "ocena bardzo dobra";
        } else if (points >= 99 && points <= 100) {
            grade = "ocena celująca";
        }
        return grade;
    }
}