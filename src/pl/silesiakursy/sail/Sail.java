package pl.silesiakursy.sail;

import java.util.Collections;
import java.util.stream.Collectors;

public class Sail {
    static int count = 5;

    public static void main(String[] args) {
        firstSolution();
        seconSolution();
        thirdSolution();
    }

    public static void firstSolution() {
        for (int i = 0; i < count; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void seconSolution() {
        for (int i = 0; i < count; i++) {
            String string = "";
            for (int j = 0; j <= i; j++) {
                string += "*";
            }
            System.out.println(string);
        }
    }

    public static void thirdSolution() {
        for (int i = 1; i <= count; i++) {
            System.out.println(Collections.nCopies(i, "*")
                    .stream()
                    .collect(Collectors.joining("")));
        }
    }
}
